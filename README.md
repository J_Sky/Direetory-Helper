### 目录比较同步助手 Direetory Helper
----------------------
 * @version1.0.0
 * @author J_Sky 
 * 本程序只实现了一些微不足道的功能，旨在学习JAVA。
 * 希望本程序能为初学者提供一些帮助，希望和一样在学习JAVA的过程中遇到迷惑的人会遇见光明。 JAVA C# php 易语言 交流
 * Q群：186960527
 * 

### 程序简介：

![输入图片说明](http://git.oschina.net/J_Sky/Direetory-Helper/raw/dir/img/a.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/J_Sky/Direetory-Helper/raw/dir/img/b.png "在这里输入图片标题")

### 版本更新
 * 2015-10-24 做一些更新：
 * 把程序的功能拆分出来，降低了程序的耦合性，把功能分离出来。
 * 创建了目录比较器类，方便后继功能的扩展。
 * 创建了文件复制线程类。
 * 优化了同步助手GUI中的一些错误。
 * 修复了一个重要的错误！

